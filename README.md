# smhttp2

simple/small http2 server

## Refs:

* https://python-hyper.org/projects/h2/en/stable/

* https://github.com/python-hyper/hyper-h2

* https://github.com/golang/net/tree/master/http2
